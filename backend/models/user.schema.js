const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

var userSchema = new mongoose.Schema(
  {
    username: { type: String, unique: true },
    password: { type: String, required: true },
    email: { type: String, required: true },
    accessLevel: { type: String },
  },
  { collection: "users" }
);

userSchema.pre("save", function (next) {
  const user = this;
  if (user.isModified("password")) {
    user.accessLevel = "basic";
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        return next("Error in the production of salt");
      }
      bcrypt.hash(user.password, salt, function (error, hash) {
        if (error) {
          return next("Error in the production of hash");
        }
        user.password = hash;
        return next();
      });
    });
  } else {
    return next();
  }
});
//osszehasonlito funkcio
userSchema.methods.comparePasswords = function (password, nx) {
  bcrypt.compare(password, this.password, function (err, isMatch) {
    nx(err, isMatch);
  });
};

module.exports = userSchema;
