const router = require("express").Router();
const mongoose = require("mongoose");
const { session } = require("passport");
const aruModel = mongoose.model("aru");
const userModel = mongoose.model("user");

//authentikációhoz
const passport = require("passport");

router.route("/login").post((req, res, next) => {
  if (!req.body.username || !req.body.password) {
    res.status(400).send("Hibas kérés, username és jelszó is szükséges.");
    return;
  }

  passport.authenticate("local", function (error, user) {
    if (error) {
      console.log(error);
      return res.status(500).send("Nagy baj van");
    }
    req.logIn(user, function (error) {
      if (error) return res.status(500).send(error);

      const copyUser = { ...user };
      delete copyUser.password;
      return res.send(copyUser);
    });
  })(req, res);
});

router.route("/logout").post((req, res, next) => {
  if (!req.isAuthenticated()) {
    console.log(req.session);
    return res.status(400).send("Nincs kijelentkeztethető felhasználó!");
  }
  req.logOut();
  return res.status(200).send("Sikeres kijelentkezés!");
});

router.route("/status").get((req, res, next) => {
  if (req.isAuthenticated()) {
    return res.status(200).send(req.session.passport);
  } else {
    return res.status(400).send("Nincs bejelentkezett felhasználó");
  }
});

router
  .route("/arukereso/:id?")
  .get((req, res) => {
    if (!req.body.nev) {
      aruModel.find((err, aruk) => {
        if (err) return res.status(500).send("DB hiba " + err);
        return res.status(200).send(aruk);
      });
    } else {
      aruModel.findOne({ nev: req.body.nev }, (err, aru) => {
        if (err) return res.status(500).send("DB hiba " + err);
        if (!aru) return res.status(400).send("Nincs ilyen áru!");
        return res.status(200).send(aru);
      });
    }
  })
  .post((req, res) => {
    if (!req.body.nev || !req.body.ar || !req.body.darab) {
      return res
        .status(400)
        .send(
          "Hiányos input!" +
            " nev:" +
            req.body.nev +
            " ar: " +
            req.body.ar +
            " darab:" +
            req.body.darab
        );
    } else {
      aruModel.findOne({ nev: req.body.nev }, (err, aru) => {
        if (err) return res.status(500).send("DB hiba " + err);
        if (aru) return res.status(400).send("Létezik már ilyen termék");
        const nAru = new aruModel({
          nev: req.body.nev,
          darab: req.body.darab,
          ar: req.body.ar,
        });
        nAru.save((error) => {
          if (error)
            return res.status(500).send("DB hiba a betöltés során " + error);
          return res.status(200).send(req.body);
        });
      });
    }
  })
  .put((req, res) => {
    if (!req.body.nev || (!req.body.ar && !req.body.darab)) {
      return res.status(400).send("Hiányos input!");
    } else {
      aruModel.findOne({ nev: req.body.nev }, (err, aru) => {
        if (err) return res.status(500).send("DB hiba " + err);
        if (!aru) return res.status(400).send("Még nincs ilyen áru");
        if (req.body.ar) aru.ar = req.body.ar;
        if (req.body.darab) aru.darab = req.body.darab;
        aru.save((error) => {
          if (error)
            return res.status(500).send("DB hiba a betöltés során " + error);
          return res.status(200).send(aru);
        });
      });
    }
  })
  .delete((req, res) => {
    if (!req.body.nev) {
      aruModel.deleteMany((err) => {
        if (err) return res.status(500).send("DB hiba " + err);
        return res.status(200).send("Törölve minden");
      });
    } else {
      aruModel.deleteOne({ nev: req.body.nev }, (err) => {
        if (err) return res.status(500).send("DB hiba " + err);
        return res.status(200).send("Aru torolve (feltéve ha volt)");
      });
    }
  });

router
  .route("/users/:id?")
  .get((req, res) => {
    if (!req.body.username) {
      userModel.find((err, users) => {
        if (err) return res.status(500).send("DB hiba " + err);
        return res.status(200).send(users);
      });
    } else {
      userModel.findOne({ nev: req.body.username }, (err, user) => {
        if (err) return res.status(500).send("DB hiba " + err);
        if (!user) return res.status(400).send("Nincs ilyen user!");
        return res.status(200).send(user);
      });
    }
  })
  .post((req, res) => {
    if (!req.body.username || !req.body.password || !req.body.email) {
      console.log();
      return res
        .status(400)
        .send(
          "Hiányos input!" +
            "id: " +
            req.body.username +
            " password: " +
            req.body.password +
            " email: " +
            req.body.email
        );
    } else {
      userModel.findOne({ username: req.body.username }, (err, user) => {
        if (err) return res.status(500).send("DB hiba " + err);
        if (user)
          return res.status(400).send("Létezik már ilyen nevű felhasználó.");
        const newUser = new userModel({
          username: req.body.username,
          email: req.body.email,
          password: req.body.password,
        });
        newUser.save((error) => {
          if (error)
            return res.status(500).send("DB hiba a betöltés során " + error);
          return res.status(200).send(req.body);
        });
      });
    }
  })
  .put((req, res) => {
    if (!req.params.id || (!req.body.username && !req.body.password)) {
      return res.status(400).send("Hiányos input!");
    } else {
      userModel.findOne({ username: req.body.id }, (err, user) => {
        if (err) return res.status(500).send("DB hiba " + err);
        if (!user) return res.status(400).send("Még nincs ilyen user");
        if (req.body.password) user.password = req.body.password;
        if (req.body.email) user.darab = req.body.darab;
        user.save((error) => {
          if (error)
            return res.status(500).send("DB hiba a betöltés során " + error);
          return res.status(200).send(user);
        });
      });
    }
  })
  .delete((req, res) => {
    if (req.params.id) {
      userModel.deleteOne({ username: req.body.username }, (err) => {
        if (err) return res.status(500).send("DB hiba " + err);
        return res.status(200).send("Törölve az adott user");
      });
    } else {
      return res
        .status(403)
        .send("Tilos minden felhasználót törölni egyszerre!");
    }
  });

router.route("/login").post((req, res, next) => {
  if ((req.body.username, req.body.password)) {
    //meghívom a passport local stratégiáját és paraméterként átadom neki a req,res objektumokat
    passport.authenticate("local", function (error, user) {
      console.log("login eredménye:", user);
      if (error) return res.status(500).send(error);
      // ezzel léptetem bele a sessionbe a felhasználót, a user objektumot utána mindig el tudom majd érni
      // req.user néven
      req.logIn(user, function (error) {
        if (error) return res.status(500).send(error);
        return res.status(200).send("Bejelentkezes sikeres");
      });
    })(req, res);
  } else {
    return res.status(400).send("Hibas keres, username es password kell");
  }
});

router.route("/logout").post((req, res, next) => {
  console.log("user:", req.user, req.session);
  // ha volt sikeres login és sikerült sessionbe léptetni a usert, akkor a session megszüntetéséig
  // vagyis logoutig ez az isAuthenticated() mindig true lesz majd
  if (req.isAuthenticated()) {
    req.logout(); // megszünteti a sessiont
    return res.status(200).send("Kijelentkezes sikeres");
  } else {
    return res.status(403).send("Nem is volt bejelentkezve");
  }
});

router.route("/addtocart?:id").post((req, res, next) => {
  //Amennyiben a felhasznalo be van jelentkezve
  if (req.isAuthenticated()) {
    res.status(200).send(aru.id);
  }
});

module.exports = router;
