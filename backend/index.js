const express = require("express");
const mongoose = require("mongoose");
const app = express();

const session = require("express-session");
const passport = require("passport");
const localStrategy = require("passport-local").Strategy;

const port = process.env.PORT || 3000;
const dbUrl =
  "mongodb+srv://admin:adminpass@prfkotrpoggzn.1fvvq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
// const dbUrl = "mongodb://localhost:27017";
mongoose.connect(dbUrl);
const cors = require("cors");
const methods = require("./models/aru.schema");
const { rawListeners } = require("./models/aru.schema");

mongoose.connection.on("connected", () => {
  console.log("db connected");
});
mongoose.connection.on("error", (err) => {
  console.log("db error", err);
});

mongoose.model("aru", require("./models/aru.schema"));
mongoose.model("user", require("./models/user.schema"));

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(cors({ credentials: true, origin: "http://localhost:4200" }));

passport.use(
  "local",
  new localStrategy(function (username, password, done) {
    const userModel = mongoose.model("user");
    userModel.findOne({ username: username }, function (err, user) {
      if (err) return done("Hiba a lekéres soran", null);
      if (!user) return done("Nincs ilyen felhasználónév", null);
      user.comparePasswords(password, function (error, isMatch) {
        if (error) return done(error, false);
        if (!isMatch) return done("Hibas jelszo", false);
        return done(null, user);
      });
    });
  })
);

passport.serializeUser(function (user, done) {
  console.log("user", user);
  if (!user) return done("nincs megadva beléptethető felhasználó", null);
  return done(null, user);
});
passport.deserializeUser(function (user, done) {
  console.log("User deserialized", user);
  if (!user) return done("nincs user akit kiléptethetnénk", null);
  return done(null, user);
});

//Express session
app.use(
  session({ secret: "akarmilyenhosszustringesbiztonsagos", resave: true })
);
//Passport inicializalasa
app.use(passport.initialize());
app.use(passport.session());

// app.use(passport.authenticate("session"));
// app.use(session());

app.use("/", require("./routes"));
app.use((req, res, next) => {
  console.log("Hibakezelo");
  res.status(404).send("Nem talalhato a lekert oldal");
});

// app.use("/login", require("./routes"));
// app.use("/logout", require("./routes"));

app.listen(port, () => {
  console.log("A szerver elindult");
});
