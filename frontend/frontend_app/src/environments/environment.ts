import * as Local from './environment.local';
import * as Prod from './environment.prod';
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const isProd = false;

export const environment = isProd ? Prod.environment : Local.environment;

export const Urls = {
  loginUrl: environment.serverUrl + '/login',
  logOutUrl: environment.serverUrl + '/logout',
  itemUrl: environment.serverUrl + '/arukereso',
  addToCartUrl: environment.serverUrl + '/addToCart',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
