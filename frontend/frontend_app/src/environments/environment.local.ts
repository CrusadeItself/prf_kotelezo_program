export const environment = {
  production: true,
  custom: 'This is the production environment',
  // serverUrl: 'https://herokuappprf.herokuapp.com/',
  serverUrl: 'http://localhost:3000',
};
