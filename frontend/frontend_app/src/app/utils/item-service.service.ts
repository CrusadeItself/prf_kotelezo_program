import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { response, text } from 'express';
import { catchError, map, Observable, of, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Urls } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ItemServiceService {
  constructor(private http: HttpClient) {}

  getItems(): Observable<Item[]> {
    console.log(Urls.itemUrl + ' KLIKK');
    console.log(this.http.get<Observable<Item[]>>(Urls.itemUrl));
    return this.http.get<Item[]>(Urls.itemUrl);
  }

  addToCart(item: Item): Observable<Item> {
    console.log(this.http.get<Item>(Urls.itemUrl + '/1'));
    return this.http.get<Item>(Urls.itemUrl + '/1');
  }
}

export interface Item {
  nev: string;
  ar: number;
  darab: number;
}
