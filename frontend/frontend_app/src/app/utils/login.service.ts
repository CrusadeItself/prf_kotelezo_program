import { environment, Urls } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private http: HttpClient) {}

  login(username: string, password: string) {
    return this.http.post(
      Urls.loginUrl,
      {
        username: username,
        password: password,
      },
      { withCredentials: true, responseType: 'text' }
    );
  }

  logout() {
    console.log('Klikk', Urls.logOutUrl, fetch);
    return this.http.post(
      Urls.logOutUrl,
      {},
      { withCredentials: true, responseType: 'text' }
    );
  }
}
