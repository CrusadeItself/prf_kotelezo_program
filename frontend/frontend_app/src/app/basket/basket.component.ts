import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css'],
})
export class BasketComponent implements OnInit {
  ujAru = '';
  basket = [''];
  constructor(private http: HttpClient, private router: Router) {}

  ngOnInit(): void {}

  backToMainPage() {
    this.router.navigate(['/mainPage']);
  }

  Order() {
    if (this.ujAru !== '' && !(this.basket.indexOf(this.ujAru) > -1)) {
      this.basket.push(this.ujAru);
      this.ujAru = '';
      return this.basket;
    } else {
      return 'Van már ilyen áru felvéve';
    }
  }

  goToMainPage() {
    this.router.navigate(['/mainPage']);
  }
}
