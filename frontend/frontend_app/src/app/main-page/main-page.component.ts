import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../utils/login.service';
import { Item, ItemServiceService } from '../utils/item-service.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css'],
})
export class MainPageComponent implements OnInit {
  constructor(
    private router: Router,
    private loginService: LoginService,
    private itemService: ItemServiceService
  ) {}
  items: Item[] = [];
  basket: Item[] = [];

  ngOnInit(): void {
    this.subscribeToItems();
  }

  async logOut() {
    await this.loginService.logout().toPromise();
    this.router.navigate(['/login']);
  }
  //get the items from the DB
  subscribeToItems(): void {
    this.itemService
      .getItems()
      .subscribe((items) => this.items.splice(0, this.items.length, ...items));
  }

  addItemToCart(item: Item): void {
    console.log(item);
    this.itemService.addToCart(item);
  }

  goTobasket() {
    this.router.navigate(['/basket']);
  }

  Order() {}
}
